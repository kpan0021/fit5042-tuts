import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { PokemonCrudComponent } from './pokemon-crud/pokemon-crud.component'
import { PokemonDetailComponent } from './pokemon-detail/pokemon-detail.component'
import { PokemonListComponent } from './pokemon-list/pokemon-list.component'

const routes: Routes = [
	{ path: 'crud-app', component: PokemonCrudComponent },
	{ path: 'pokemons', component: PokemonListComponent },
	{ path: 'pokemon/:id', component: PokemonDetailComponent }
]

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule { }
