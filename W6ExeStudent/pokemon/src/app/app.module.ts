import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './app.component'
import { PokemonCrudComponent } from './pokemon-crud/pokemon-crud.component'
import { InMemoryWebApiModule } from 'angular-in-memory-web-api'
import { PokemonDataService } from './data/PokemonDataService'
import { HttpClientModule } from '@angular/common/http';
import { PokemonListComponent } from './pokemon-list/pokemon-list.component';
import { PokemonDetailComponent } from './pokemon-detail/pokemon-detail.component'

@NgModule({
	declarations: [
		AppComponent,
		PokemonCrudComponent,
		PokemonListComponent,
		PokemonDetailComponent
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		FormsModule,
		ReactiveFormsModule,
		InMemoryWebApiModule.forRoot(PokemonDataService, {
			passThruUnknownUrl: true
		}),
		HttpClientModule
	],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule { }
