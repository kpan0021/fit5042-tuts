package fit5042.tutex.calculator;

import java.util.ArrayList;

import javax.ejb.Stateful;

import fit5042.tutex.repository.entities.Property;

@Stateful
public class ComparePropertySessionBean implements CompareProperty {
	ArrayList<Property> allproperties;

	public ComparePropertySessionBean() {
		this.allproperties = new ArrayList<Property>();
	}
	@Override
	public void addProperty(Property property) {
		this.allproperties.add(property);
	}

	@Override
	public void removeProperty(Property property) {
		int index = this.allproperties.indexOf(property);
		this.allproperties.remove(index);
	}

	@Override
	public int getBestPerRoom() {
		int id = 0;
		double bestPerRoom = 99999.0;
		for(Property p: allproperties) {
			int numberOfRooms = p.getNumberOfBedrooms();
			double price = p.getPrice();
			double pricePerRoom = price / numberOfRooms;
			if(pricePerRoom < bestPerRoom) {
				bestPerRoom = pricePerRoom;
				id = p.getPropertyId(); 
			}
		}
		return id;
	}

}
