package mortgagecalculator;

import java.io.Serializable;

import javax.faces.bean.SessionScoped;
import javax.inject.Named;

import mortgagecalculator.client.WebServiceClient;

@Named(value = "webServiceBean")
@SessionScoped
public class WebServiceBean implements Serializable {
	private int year;
	private double interestRate, principle;
	private WebServiceClient webServiceClient;
	
	public WebServiceBean() {
	}
	
	
	public double getPrinciple() {
		return principle;
	}
	
	public void setPrinciple(double principle) {
		this.principle = principle;
	}
	
	public double getInterestRate() {
		return interestRate;
	}
	
	public void setInterestRate(double interestRate) {
		this.interestRate = interestRate;
	}
	
	public int getYear() {
		return year;
	}
	
	public void setYear(int year) {
		this.year = year;
	}
	
	public void setWebServiceClient() {
		webServiceClient = new WebServiceClient();
		int year = getYear();
		double principle = getPrinciple();
		double interestRate = getInterestRate();
		webServiceClient.setCalculationFormData(year, interestRate, principle);
	}

}
