package mortgagecalculator;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;

@Path("mortgage")
public class MortgageCalculator {
	@SuppressWarnings("unused")
	@Context
	private UriInfo context;

	@EJB
	private CalculatorBean mortgageCalculatorBean;

	/**
	 * Default constructor.
	 */
	public MortgageCalculator() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Retrieves representation of an instance of MortgageCalculator
	 * 
	 * @return an instance of String
	 */
	@GET
	@Produces("text/html")
	public String getHtml() {
		return "<p>" + mortgageCalculatorBean.getMonthlyPayment() + "</p>";
	}

	@POST
	@Consumes("application/x-www-form-urlencoded")
	public void setDataForCalculation(@FormParam("interestRate") double interestRate,
			@FormParam("principle") double principle, @FormParam("year") int year) {
		System.out.println("setting data in calculator bean");
		System.out.println("BEAN " + Integer.toString(year));
		System.out.println("BEAN " + Double.toString(interestRate));
		System.out.println("BEAN " + Double.toString(principle));
		CalculatorBean bean = new CalculatorBean(year, interestRate, principle);
	}
}