package mortgagecalculator;

import javax.ejb.LocalBean;
import javax.ejb.Singleton;

/**
 * Session Bean implementation class CalculatorBean
 */
@Singleton
@LocalBean
public class CalculatorBean {
	private double principle, interestRate;
	private int year;
	private double monthlyPayment = 0.0;
	
	/**
	 * Default Constructor
	 */
	public CalculatorBean() {
		
	}
	
    public CalculatorBean(int year, double interestRate, double principle) {
    	this.year = year;
    	this.interestRate = interestRate;
    	this.principle = principle;
    	this.calculate();
    }
    
    public double getPrinciple() {
		return this.principle;
	}
	
	public double getInterestRate() {
		return this.interestRate;
	}
	
	public int getYear() {
		return this.year;
	}
	
	public double getMonthlyPayment() {
		System.out.println("getMonthlyPayment " + Double.toString(this.monthlyPayment));
		return this.monthlyPayment;
	}
	
	public void setMonthlyPayment(double value) {
		this.monthlyPayment = value;
	}
    
    public void calculate() {
        int numberOfPayments = this.year * 12;
        double monthlyPayment = 0.0;
        if (this.interestRate > 0) {
        	System.out.println("----->>>>>");
            monthlyPayment = this.principle * (this.interestRate * (Math.pow((1 + this.interestRate), numberOfPayments))) / ((Math.pow((1 + this.interestRate), numberOfPayments)) - 1);
        } else {
            monthlyPayment = 1000;
        }
        this.setMonthlyPayment(monthlyPayment);
    }

}
