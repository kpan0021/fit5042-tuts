/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.tutex.mbeans;

import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.enterprise.context.SessionScoped;

import fit5042.tutex.repository.entities.Loan;
import fit5042.tutex.calculator.MonthlyPaymentCalculator;
import fit5042.tutex.calculator.MonthlyPaymentCalculatorSessionBean;

/**
 *
 * @author: originally created by Eddie. The following code has been changed in
 *          order for students to practice.
 */
@ManagedBean(name = "loanManagedBean", eager = true)
@SessionScoped
public class LoanManagedBean implements Serializable {

	private Loan loan;

	public LoanManagedBean() {
		this.loan = new Loan();

	}

	public Loan getLoan() {
		return loan;
	}

	public void setLoan(Loan loan) {
		this.loan = loan;
	}

	public String calculate() {	
		double monthlyPayment;
		MonthlyPaymentCalculatorSessionBean paymentCalculator = new MonthlyPaymentCalculatorSessionBean();
		double principle = this.loan.getPrinciple();
		int year = this.loan.getNumberOfYears();
		double interestRate = this.loan.getInterestRate();
		monthlyPayment = paymentCalculator.calculate(principle, year, interestRate);
		this.loan.setMonthlyPayment(monthlyPayment);
		return "index";
	}
}
